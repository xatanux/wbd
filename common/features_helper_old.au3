#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         Xatanú

 Script Function:
    Functions for interacting with "Windows Optional Features"

#ce ----------------------------------------------------------------------------

#include-once "logger_helper.au3"
#include-once "dialogs_helper.au3"

Global $aFeaturesList[0][3]

Func _SetFeatureState($featureid, $enable)
   If ($enable) Then
	  $param = "/enable-feature /all"
   Else
	  $param = "/disable-feature"
   EndIf

   Local $iPID = Run("Dism /online " & $param & " /featurename:" & $featureid & " /English /NoRestart", "", @SW_HIDE, $STDOUT_CHILD)
   $res = ProcessWaitClose($iPID)
   $exitcode = @extended
   If ($res) Then
	  Local $sOutput = StdoutRead($iPID)
	  ;_WriteLineToLog($exitcode & " - " & $sOutput)
	  If (StringInStr($sOutput, "The operation completed successfully")) Then Return True
   EndIf
   Return False
EndFunc

Func _EnableFeature($featureid)
   _WriteLineToLog("Please, wait while enabling feature: " & $featureid)
   $hWaitDlg = _ShowWait_Dlg($hGUI, "Please, wait while enabling the feature")
   $res = _SetFeatureState($featureid, True)
   _HideWait_Dlg($hWaitDlg, $hGUI)
   If ($res) Then
	  _WriteLineToLog("Feature enabled: " & $featureid)
   Else
	  _WriteLineToLog("Error enabling feature: " & $featureid, $LOG_ERROR)
   EndIf
   Return $res
EndFunc

Func _DisableFeature($featureid)
   _WriteLineToLog("Please, wait while disabling feature: " & $featureid)
   $hWaitDlg = _ShowWait_Dlg($hGUI, "Please, wait while disabling the feature")
   Local $res = _SetFeatureState($featureid, False)
   _HideWait_Dlg($hWaitDlg, $hGUI)
   If ($res) Then
	  _WriteLineToLog("Feature disabled: " & $featureid)
   Else
	  _WriteLineToLog("Error disabling feature: " & $featureid, $LOG_ERROR)
   EndIf
   Return $res
EndFunc

Func GetFeatureInfo($featureid)
   Local $is_enabled
   Local $display_info
   Local $iPID = Run("Dism /online /get-featureinfo /featurename:"& $featureid & " /English", "", @SW_HIDE, $STDOUT_CHILD)
   $res = ProcessWaitClose($iPID)
   $exitcode = @extended
   If ($res) Then
	  Local $sOutput = StdoutRead($iPID)
	  ;_WriteLineToLog($exitcode & " - " & $sOutput)
	  If StringInStr($sOutput, "Error: ") > 0 Or StringInStr($sOutput, "A Windows feature name was not recognized") > 0 Then
		 SetError(1)
	  Else
		 $startPos = StringInStr($sOutput, "Description : ")
		 If ($startPos) Then
			$startPos += StringLen("Description : ")
			$endPos = StringInStr($sOutput, @CR, 0, 1, $startPos)
			$display_info = StringMid($sOutput, $startPos, $endPos-$startPos)
			;_WriteLineToLog($display_info)
		 EndIf
		 $is_enabled = StringInStr($sOutput, "State : Enabled") > 0
	  EndIf
   EndIf
   Local $result = [$display_info, _DisplayIsFeatureEnabled($is_enabled)]
   Return $result
EndFunc

Func _GetFeatureStatus($featureid)
   If (UBound($aFeaturesList) == 0) Then
	  _EnumAllFeatures()
   EndIf
   Local $iIndex = _ArraySearch($aFeaturesList, $featureid)
   If ($iIndex == -1) Then
	  SetError(1)
	  Return -1
   Else
	  Return $aFeaturesList[$iIndex][1]
   EndIf
EndFunc

Func _DisplayIsFeatureEnabled($feature_is_enabled)
   If ($feature_is_enabled) Then
	  Return "Enabled"
   Else
	  Return "Disabled"
   EndIf
EndFunc

Func _IsFeatureEnabled($strFeatureIsEnabled)
   If ($strFeatureIsEnabled == "Enabled") Then Return True
   Return False
EndFunc

Func _EnumAllFeatures($bQueryDisplayName=False)
   Global $aFeaturesList[0][3]
   $hWaitDlg = _ShowWait_Dlg($hGUI, "Please, wait while checking all the features." & @CR & "This may take some minutes")
   Local $iPID = Run("Dism /online /English /get-features /format:table", "", @SW_HIDE, $STDOUT_CHILD)
   $res = ProcessWaitClose($iPID)
   $exitcode = @extended
   If ($res) Then
	  Local $sOutput = StdoutRead($iPID)
	  ;_WriteLineToLog($exitcode & " - " & $sOutput)
	  $aOutput = StringSplit($sOutput, @CR)
	  For $i = 1 To UBound($aOutput) - 1
		 $strCurLine = $aOutput[$i]
		 $aCurLine = StringSplit($strCurLine, "|")
		 If (IsArray($aCurLine) And UBound($aCurLine) == 3) Then
			If (StringInStr($aCurLine[1], "-------------") == 0 And StringInStr($aCurLine[1], "Feature Name") == 0) Then
			   $service_name = StringStripWS($aCurLine[1], $STR_STRIPLEADING + $STR_STRIPTRAILING)
			   $service_status = StringStripWS($aCurLine[2], $STR_STRIPLEADING + $STR_STRIPTRAILING)
			   If ($service_status <> "Enabled") Then $service_status = "Disabled"
			   If ($bQueryDisplayName) Then
				  $service_display_name = GetFeatureInfo($service_name)[0]
			   Else
				  $service_display_name = ""
			   EndIf
			   _ArrayAdd($aFeaturesList, $service_name&"|"&$service_status&"|"&$service_display_name)
			EndIf
		 EndIf
	  Next
   EndIf
   _HideWait_Dlg($hWaitDlg, $hGUI)
   Return $aFeaturesList
EndFunc