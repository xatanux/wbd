#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         Xatanú

 Script Function:
    Functions for interacting with "Windows Tasks"

#ce ----------------------------------------------------------------------------

#include <MsgBoxConstants.au3>
#include "../libs/taskplanerCOM.au3"
#include-once "logger_helper.au3"

; #FUNCTION# =======================================================================================================================================================
; Name...........: _DisableTask
; Description ...: Disable a Windows task
; Syntax.........: _DisableTask($task_name)
; Parameters ....: $task_name - Path and name of the task
; Requirement(s).: Administration rights
; Return values .: Success - 1 - Task has been disabled
;                  Failure - 0 - Couldn't disable the task
;                            Sets @error
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: _DisableTask("\Microsoft\Windows\Autochk\Proxy")
; ==================================================================================================================================================================
Func _DisableTask($task_name)
   $res = _TaskDisable($task_name)
   If ($res) Then
	  _WriteLineToLog("Task disabled: " & $task_name)
   Else
	  _WriteLineToLog("Error disabling task: " & $task_name, $LOG_ERROR)
	  ;MsgBox($MB_SYSTEMMODAL, "Title", "Error disabling task: " & $task_name)
   EndIf
   Return $res
EndFunc ;==> _DisableTask

; #FUNCTION# =======================================================================================================================================================
; Name...........: _EnableTask
; Description ...: Enable a Windows task
; Syntax.........: _EnableTask($task_name)
; Parameters ....: $task_name - Path and name of the task
; Requirement(s).: Administration rights
; Return values .: Success - 1 - Task has been enabled
;                  Failure - 0 - Couldn't enable the task
;                            Sets @error
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: _EnableTask("\Microsoft\Windows\Autochk\Proxy")
; ==================================================================================================================================================================
Func _EnableTask($task_name)
   $res = _TaskEnable($task_name)
   If ($res) Then
	  _WriteLineToLog("Task enabled: " & $task_name)
   Else
	  _WriteLineToLog("Error enabling task: " & $task_name, $LOG_ERROR)
	  ;MsgBox($MB_SYSTEMMODAL, "Title", "Error enabling task: " & $task_name)
   EndIf
   Return $res
EndFunc ;==> _EnableTask

; #FUNCTION# =======================================================================================================================================================
; Name...........: _DisplayIsTaskEnabled
; Description ...: Return the literal to show for the Task status
; Syntax.........: _DisplayIsTaskEnabled($task_is_enabled)
; Parameters ....: $task_is_enabled - True/False Task Enabled/Disabled
; Requirement(s).:
; Return values .: Success - "Enabled"
;                            "Disabled"
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: _DisplayIsTaskEnabled(True) will return "Enabled"
; ==================================================================================================================================================================
Func _DisplayIsTaskEnabled($task_is_enabled)
   If ($task_is_enabled) Then
	  Return "Enabled"
   Else
	  Return "Disabled"
   EndIf
EndFunc ;==> _DisplayIsTaskEnabled

; #FUNCTION# =======================================================================================================================================================
; Name...........: _IsTaskRunning
; Description ...: Check if the task is running
; Syntax.........: _IsTaskRunning($task_name)
; Parameters ....: $task_name - Path and task name
; Requirement(s).: Administrative rights
; Return values .: Success - True - Task is running
;                            False - Task is not running (or error checking)
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _IsTaskRunning($task_name)
   If (_TaskIsRunning($task_name) == 1) Then Return True
   Return False
EndFunc ;==> _IsTaskRunning

; #FUNCTION# =======================================================================================================================================================
; Name...........: _DisplayTaskRunning
; Description ...: Return the literal to show for the Task status
; Syntax.........: _DisplayTaskRunning($task_is_running)
; Parameters ....: $task_is_running - True/False Task Running/Stopped
; Requirement(s).:
; Return values .: Success - "Running"
;                            "Stopped"
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: _DisplayTaskRunning(True) will return "Running"
; ==================================================================================================================================================================
Func _DisplayTaskRunning($task_is_running)
   If ($task_is_running) Then
	  Return "Running"
   Else
	  Return "Stopped"
   EndIf
EndFunc ;==> _DisplayTaskRunning

; #FUNCTION# =======================================================================================================================================================
; Name...........: _EnumAllTasks
; Description ...: Enum all the available Tasks in the system
; Syntax.........: _EnumAllTasks([$folder], [$hidden])
; Parameters ....: $folder - Tasks folder to start searching
;                  $hidden - 1=Retrieve hidden tasks / 0=Don't retrieve hidden tasks
; Requirement(s).: Administration rights
; Return values .: Success - Array with the tasks information with the following structure: task_name|task_is_enabled|task_description
;                  Failure - Empty array
;                            Sets @error
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......: _EnumAllTasksRec
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _EnumAllTasks($folder = "\", $hidden = 1)
   Global $aTasksList[0][3]
   Local $oService

   If (Not _TaskIsValidPlatfrom()) Then
	  SetError(1)
   Else
	  $oService = ObjCreate("Schedule.Service")
	  $oService.Connect()
	  $aTasksList = _EnumAllTasksRec($oService, $folder, $hidden)

	  ; Delete object
	  $oService = 0

	  If ($ComErrorFound) Then
		 $ComErrorFound = 0
		 SetError(1)
	  EndIf
   EndIf

   Return $aTasksList
EndFunc ;==> _EnumAllTasks

; #FUNCTION# =======================================================================================================================================================
; Name...........: _EnumAllTasksRec
; Description ...: Complement for the _EnumAllTasks function. This is the recursive function to navigate through all the subfolders structure
; Syntax.........: _EnumAllTasksRec($oService, $folder = "\", $hidden = 1)
; Parameters ....: $oService - Windows Scheduler Object
;                  $folder - Tasks folder to start searching
;                  $hidden - 1=Retrieve hidden tasks / 0=Don't retrieve hidden tasks
; Requirement(s).: Administration rights
; Return values .: Success - Array with the tasks information with the following structure: task_name|task_is_enabled|task_description
;                  Failure - Empty array
;                            Sets @error
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......: _EnumAllTasks
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _EnumAllTasksRec($oService, $folder = "\", $hidden = 1)
   Local $oFolder
   Local $aTasksList[0][3]

   $oFolder = $oService.GetFolder($folder)
   If (IsObj($oFolder) And Not $ComErrorFound) Then
	  $oFolderList = $oFolder.GetFolders($hidden)
	  For $objFolder in $oFolderList
		 _ArrayAdd($aTasksList, _EnumAllTasksRec($oService, $objFolder.Path(), $hidden))
	  Next

	  ; ConsoleWrite($folder & @CR)
	  $oTasks = $oFolder.GetTasks($hidden)
	  If IsObj($oTasks) And Not $ComErrorFound Then
		 For $objItem In $oTasks
			$strTaskName = $objItem.Path()
			$strTaskDescription = $objItem.Definition()
			$strTaskEnabled = _DisplayIsTaskEnabled($objItem.Enabled())
			_ArrayAdd($aTasksList, $strTaskName&"|"&$strTaskEnabled&"|"&$strTaskDescription)
		 Next
	  EndIf
   EndIf

   Return $aTasksList
EndFunc ;==> _EnumAllTasksRec