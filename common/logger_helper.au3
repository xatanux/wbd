#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         Xatanú

 Script Function:
    Functions for logging information

#ce ----------------------------------------------------------------------------

#include <Date.au3>
#include <GuiRichEdit.au3>

Global $LOG_ERROR = 0
Global $LOG_WARNING = 1
Global $LOG_WARN = $LOG_WARNING
Global $LOG_INFO = 2
Global $LOG_DEBUG = 3

Global $LOG_LEVEL = $LOG_INFO

Dim $LOG_ERROR_COLOR = 0xFF0000
Dim $LOG_WARN_COLOR = 0xFF8000
Dim $LOG_INFO_COLOR = 0x000000
Dim $LOG_DEBUG_COLOR = 0x0000FF

; #FUNCTION# =======================================================================================================================================================
; Name...........: _WriteLineToLog
; Description ...: Writes text in the logger window with coloured text based on the logging level
; Syntax.........: _WriteLineToLog($strText, [$iLogLevel])
; Parameters ....: $strText - Text to write
;                  [$iLogLevel] - Logging level [$LOG_ERROR, $LOG_WARNING, $LOG_INFO, $LOG_DEBUG]
; Requirement(s).:
; Return values .:
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......: _GUICtrlRichEdit_WriteLine
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _WriteLineToLog($strText, $iLogLevel=$LOG_INFO)
   If ($iLogLevel <= $LOG_LEVEL) Then
	  Local $strLogLevel
	  Local $log_color
	  Switch $iLogLevel
		 Case $LOG_INFO
			$strLogLevel = "INFO"
			$log_color = $LOG_INFO_COLOR
		 Case $LOG_WARNING
			$strLogLevel = "WARNING"
			$log_color = $LOG_WARN_COLOR
		 Case $LOG_ERROR
			$strLogLevel = "ERROR"
			$log_color = $LOG_ERROR_COLOR
		 Case $LOG_DEBUG
			$strLogLevel = "DEBUG"
			$log_color = $LOG_DEBUG_COLOR
		 Case Else
			$strLogLevel = "UNKNOWN"
			$log_color = $LOG_INFO_COLOR
	  EndSwitch
	  ;GUICtrlSetData($Log_Edit, _NowCalc() & " - [" & $strLogLevel & "] - " & $strText & @CRLF, 1)
	  _GUICtrlRichEdit_WriteLine($Log_Edit, _NowCalc() & " - [" & $strLogLevel & "] - " & $strText, 0, "", $log_color)
   EndIf
Endfunc ;==> _WriteLineToLog

; #FUNCTION# =======================================================================================================================================================
; Name...........: _GUICtrlRichEdit_WriteLine
; Description ...: Write coloured text to the Rich Edit Control
; Syntax.........: _GUICtrlRichEdit_WriteLine($hWnd, $sText, [$iIncrement], [$sAttrib], [$iColor])
; Parameters ....: $hWnd - Handle to the Rich Edit Control
;                  $sText - Text to write
; Requirement(s).:
; Return values .:
; Author ........: Melba23 ?
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _GUICtrlRichEdit_WriteLine($hWnd, $sText, $iIncrement = 0, $sAttrib = "", $iColor = -1)
    ; Count the @CRLFs
    StringReplace(_GUICtrlRichEdit_GetText($hWnd, True), @CRLF, "")
    Local $iLines = @extended
    ; Adjust the text char count to account for the @CRLFs
    Local $iEndPoint = _GUICtrlRichEdit_GetTextLength($hWnd, True, True) - $iLines

    ; Add new text
    _GUICtrlRichEdit_AppendText($hWnd, $sText & @CRLF) ; @CRLF added by the function <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    ; Select text between old and new end points
    _GUICtrlRichEdit_SetSel($hWnd, $iEndPoint, -1)
    ; Convert colour from RGB to BGR
    $iColor = Hex($iColor, 6)
    $iColor = '0x' & StringMid($iColor, 5, 2) & StringMid($iColor, 3, 2) & StringMid($iColor, 1, 2)
    ; Set colour
    If $iColor <> -1 Then _GUICtrlRichEdit_SetCharColor($hWnd, $iColor)
    ; Set size
    If $iIncrement <> 0 Then _GUICtrlRichEdit_ChangeFontSize($hWnd, $iIncrement)
    ; Set weight
    If $sAttrib <> "" Then _GUICtrlRichEdit_SetCharAttributes($hWnd, $sAttrib)
    ; Clear selection
    _GUICtrlRichEdit_Deselect($hWnd)
EndFunc   ;==>_GUICtrlRichEdit_WriteLine