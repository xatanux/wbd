#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         Xatanú

 Script Function:
    Functions for interacting with "Windows Services and Drivers"

#ce ----------------------------------------------------------------------------

#include <MsgBoxConstants.au3>
#include "../libs/Services.au3"
#include-once "logger_helper.au3"

; #FUNCTION# =======================================================================================================================================================
; Name...........: _DisplayServiceStatus
; Description ...: Tanslate Windows service status to literal
; Syntax.........: _DisplayServiceStatus($status)
; Parameters ....: $status - status internal value as returned by windows
; Requirement(s).:
; Return values .: Success - String with the corresponding status [Stopped, Start Pending, Stop Pending, Running, Continue Pending, Pause Pending, Paused, Invalid Status]
;                  Failure - "Invalid"
;                            Sets @error
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _DisplayServiceStatus($status)
   Local $status_literal = $status
   If (IsArray($status)) Then
	  Switch $status[1]
		 Case $SERVICE_STOPPED
			$status_literal = "Stopped"
		 Case $SERVICE_START_PENDING
			$status_literal = "Start Pending"
		 Case $SERVICE_STOP_PENDING
			$status_literal = "Stop Pending"
		 Case $SERVICE_RUNNING
			$status_literal = "Running"
		 Case $SERVICE_CONTINUE_PENDING
			$status_literal = "Continue Pending"
		 Case $SERVICE_PAUSE_PENDING
			$status_literal = "Pause Pending"
		 Case $SERVICE_PAUSED
			$status_literal = "Paused"
		 Case Else
			$status_literal = "Invalid"
			SetError(1)
	  EndSwitch
   EndIf
   Return $status_literal
EndFunc ;==> _DisplayServiceStatus

; #FUNCTION# =======================================================================================================================================================
; Name...........: _IsServiceStopped
; Description ...: Check if the service is stopped based on the status returned by windows
; Syntax.........: _IsServiceStopped($status)
; Parameters ....: $status - Internal windows service status [$SERVICE_STOPPED, $SERVICE_START_PENDING, $SERVICE_STOP_PENDING, $SERVICE_RUNNING, $SERVICE_CONTINUE_PENDING, $SERVICE_PAUSE_PENDING, $SERVICE_PAUSED]
; Requirement(s).:
; Return values .: Success - True - Service is stopped
;                            False - Service is not stopped
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _IsServiceStopped($status)
   Return $status[1] == $SERVICE_STOPPED
EndFunc ;==> _IsServiceStopped

; #FUNCTION# =======================================================================================================================================================
; Name...........: _IsServiceStoppedStr
; Description ...: Check if the service is stopped based on the status returned by _DisplayServiceStatus
; Syntax.........: _IsServiceStoppedStr($status)
; Parameters ....: $status - Service status literal [Stopped, Start Pending, Stop Pending, Running, Continue Pending, Pause Pending, Paused, Invalid Status]
; Requirement(s).:
; Return values .: Success - True - Service is stopped
;                            False - Service is not stopped
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......: _DisplayServiceStatus
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _IsServiceStoppedStr($status)
   Return $status == "Stopped"
EndFunc ;==> _IsServiceStoppedStr

; #FUNCTION# =======================================================================================================================================================
; Name...........: _IsServiceRunning
; Description ...: Check if the service is running based on the status returned by windows
; Syntax.........: _IsServiceRunning($status)
; Parameters ....: $status - Internal windows service status [$SERVICE_STOPPED, $SERVICE_START_PENDING, $SERVICE_STOP_PENDING, $SERVICE_RUNNING, $SERVICE_CONTINUE_PENDING, $SERVICE_PAUSE_PENDING, $SERVICE_PAUSED]
; Requirement(s).:
; Return values .: Success - True - Service is running
;                            False - Service is not running
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _IsServiceRunning($status)
   Return $status[1] == $SERVICE_RUNNING
EndFunc ;==> _IsServiceRunning

; #FUNCTION# =======================================================================================================================================================
; Name...........: _DisplayServiceStartType
; Description ...: Translate Windows service StartUp configuration to literal for display
; Syntax.........: _DisplayServiceStartType($start_type)
; Parameters ....: $start_type - Internal windows service startup config [$SERVICE_BOOT_START, $SERVICE_SYSTEM_START, $SERVICE_AUTO_START, $SERVICE_DEMAND_START, $SERVICE_DISABLED]
; Requirement(s).:
; Return values .: Success - String with the corresponding startup config [Boot Start, System Start, Automatic, Manual, Disabled]
;                  Failure - "Invalid"
;                            Sets @error
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _DisplayServiceStartType($start_type)
   Local $start_type_literal
   Switch $start_type
	  Case $SERVICE_BOOT_START
		 $start_type_literal = "Boot Start"
	  Case $SERVICE_SYSTEM_START
		 $start_type_literal = "System Start"
	  Case $SERVICE_AUTO_START
		 $start_type_literal = "Automatic"
	  Case $SERVICE_DEMAND_START
		 $start_type_literal = "Manual"
	  Case $SERVICE_DISABLED
		 $start_type_literal = "Disabled"
	  Case Else
		 $start_type_literal = "Invalid"
		 SetError(1)
   EndSwitch
   Return $start_type_literal
EndFunc ;==> _DisplayServiceStartType

; #FUNCTION# =======================================================================================================================================================
; Name...........: _GetIdServiceStartType
; Description ...: Translate startup config literal into Windows internal value
; Syntax.........: _GetIdServiceStartType($start_type_literal)
; Parameters ....: $start_type_literal - Literal string for the service startup config [Boot Start, System Start, Automatic, Manual, Disabled]
; Requirement(s).:
; Return values .: Success - Windows id with the corresponding startup config [$SERVICE_BOOT_START, $SERVICE_SYSTEM_START, $SERVICE_AUTO_START, $SERVICE_DEMAND_START, $SERVICE_DISABLED]
;                  Failure - -1
;                            Sets @error
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _GetIdServiceStartType($start_type_literal)
   Local $start_type_id
   Switch $start_type_literal
	  Case "Boot Start"
		 $start_type_id = $SERVICE_BOOT_START
	  Case "System Start"
		 $start_type_id = $SERVICE_SYSTEM_START
	  Case "Automatic"
		 $start_type_id = $SERVICE_AUTO_START
	  Case "Manual"
		 $start_type_id = $SERVICE_DEMAND_START
	  Case "Disabled"
		 $start_type_id = $SERVICE_DISABLED
	  Case Else
		 $start_type_id = -1
		 SetError(1)
   EndSwitch
   Return $start_type_id
EndFunc ;==> _GetIdServiceStartType

; #FUNCTION# =======================================================================================================================================================
; Name...........: _IsServiceDisabled
; Description ...: Check if the service is disabled based on the startup type returned by windows
; Syntax.........: _IsServiceDisabled($start_type)
; Parameters ....: $start_type - Internal windows startup status [$SERVICE_BOOT_START, $SERVICE_SYSTEM_START, $SERVICE_AUTO_START, $SERVICE_DEMAND_START, $SERVICE_DISABLED]
; Requirement(s).:
; Return values .: Success - True - Service is disabled
;                            False - Service is not disabled
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _IsServiceDisabled($start_type)
   Return $start_type == $SERVICE_DISABLED
EndFunc ;==> _IsServiceDisabled

; #FUNCTION# =======================================================================================================================================================
; Name...........: _IsStartTypeDemand
; Description ...: Check if the service startup is "on demand" ("manual")
; Syntax.........: _IsStartTypeDemand($start_type)
; Parameters ....: $start_type - Internal windows startup status [$SERVICE_BOOT_START, $SERVICE_SYSTEM_START, $SERVICE_AUTO_START, $SERVICE_DEMAND_START, $SERVICE_DISABLED]
; Requirement(s).:
; Return values .: Success - True - Service startup is manual (on demand)
;                            False - Service startup is not manual
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _IsStartTypeDemand($start_type)
   Return $start_type == $SERVICE_DEMAND_START
EndFunc ;==> _IsStartTypeDemand

; #FUNCTION# =======================================================================================================================================================
; Name...........: _DisableService
; Description ...: Disable a service and optionally stop it as well
; Syntax.........: _DisableService($serviceid, [$stop_service])
; Parameters ....: $serviceid - Identifier name of the service
;                  $stop_service - Stop the service after disabling it [True/False]
; Requirement(s).: Administration rights
; Return values .: Success - True - Service has been disabled
;                  Failure - False - Couldn't disable the service
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _DisableService($serviceid, $stop_service=False)
   If (_Service_SetStartType($serviceid, $SERVICE_DISABLED)) Then
	  _WriteLineToLog("Service disabled: " & $serviceid)
   Else
	  If (RegWrite("HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\" & $serviceid, "Start", "REG_DWORD", $SERVICE_DISABLED)) Then
		 _WriteLineToLog("Service disabled using registry: " & $serviceid)
	  Else
		 _WriteLineToLog("Error disabling service: " & $serviceid, $LOG_ERROR)
		 ;MsgBox($MB_SYSTEMMODAL, "Title", "Error disabling service: " & $serviceid)
		 Return False
	  EndIf
   EndIf
   If ($stop_service And _IsServiceRunning(_Service_QueryStatus($serviceid))) Then
	  If (_Service_Stop($serviceid)) Then
		 _WriteLineToLog("Service stopped: " & $serviceid)
	  Else
		 _WriteLineToLog("Couldn't stop service: " & $serviceid, $LOG_WARNING)
	  EndIf
   EndIf
   Return True
EndFunc ;==> _DisableService

; #FUNCTION# =======================================================================================================================================================
; Name...........: _EnableService
; Description ...: Enable a service and optionally start it as well
; Syntax.........: _EnableService($serviceid, $stock_startup, [$start_service])
; Parameters ....: $serviceid - Identifier name of the service
;                  $stock_startup - New startup type to setup for the service
;                  $stop_service - Start the service after disabling it [True/False]
; Requirement(s).: Administration rights
; Return values .: Success - True - Service has been enabled
;                  Failure - False - Couldn't enable the service
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _EnableService($serviceid, $stock_startup, $start_service=False)
   $stock_startup_id = _GetIdServiceStartType($stock_startup)
   If (_Service_SetStartType($serviceid, $stock_startup_id)) Then
	  _WriteLineToLog("Service enabled: " & $serviceid)
   Else
	  If (RegWrite("HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\" & $serviceid, "Start", "REG_DWORD", $stock_startup_id)) Then
		 _WriteLineToLog("Service enabled using registry: " & $serviceid)
	  Else
		 _WriteLineToLog("Error enabling service: " & $serviceid, $LOG_ERROR)
		 MsgBox($MB_SYSTEMMODAL, "Title", "Error enabling service: " & $serviceid)
		 Return False
	  EndIf
   EndIf
   If ($start_service And _IsServiceStopped(_Service_QueryStatus($serviceid))) Then
	  If (_Service_Start($serviceid)) Then
		 _WriteLineToLog("Service started: " & $serviceid)
	  Else
		 _WriteLineToLog("Couldn't start service: " & $serviceid, $LOG_WARNING)
	  EndIf
   EndIf
   Return True
EndFunc ;==> _EnableService

; #FUNCTION# =======================================================================================================================================================
; Name...........: _EnumAllServices
; Description ...: Enum all the available services or drivers in the system
; Syntax.........: _EnumAllServices($iServiceType)
; Parameters ....: $iServiceType - Windows component type to enum "Service"/"Driver"
; Requirement(s).: Administration rights
; Return values .: Success - Array with the services information with the following structure: serviceid|service_startup_type|service_display
;                  Failure - Empty array
;                            Sets @error
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _EnumAllServices($iServiceType)
   Local $aServicesList[0][3]
   Local $strServiceType = ""
   Switch $iServiceType
	  Case $SERVICE_WIN32
		 $strServiceType = "Service"
	  Case $SERVICE_DRIVER
		 $strServiceType = "Driver"
	  Case Else
		 _WriteLineToLog("Unsupported service type", $LOG_ERROR)
		 SetError(1)
		 Return $aServicesList
   EndSwitch

   $aResList = _Service_Enum($iServiceType)
   If (Not @error And IsArray($aResList)) Then
	  For $i = 1 To $aResList[0][0]
		 $serviceid = $aResList[$i][0]
		 $service_display = $aResList[$i][1]
		 $service_startup_type = _DisplayServiceStartType(_Service_QueryStartType($serviceid))
		 _ArrayAdd($aServicesList, $serviceid&"|"&$service_startup_type&"|"&$service_display)
	  Next
   EndIf
   Return $aServicesList
EndFunc ;==> _EnumAllServices