#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         Xatanú

 Script Function:
    Functions for displaying modal dialogs

#ce ----------------------------------------------------------------------------

Global $hexServices = 0x1000
Global $hexDrivers = 0x0100
Global $hexTasks = 0x0010
Global $hexFeatures = 0x0001

; #FUNCTION# =======================================================================================================================================================
; Name...........: _CreateNewFile_Dlg
; Description ...: Dialog to pick which components to include in the new services.txt file to be generated
; Syntax.........: _CreateNewFile_Dlg($parent)
; Parameters ....: $parent - Handle to the main window
; Requirement(s).:
; Return values .: Success - Hex value that represent which options have been checked. All options checked: 0x1111. No options checked: 0x0000
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _CreateNewFile_Dlg($parent)
   Local $iDialog_Width = 300
   Local $iMsg_Height = 100
   Local $iHpos = -1
   Local $iVPos = -1
   Local $iStyle = BitOR(0x80880000, 0x00C00000)
   Local $iExtStyle = -1
   Local $strWndTitle = "Create new services list"

   Local $hMsgGUI = GUICreate($strWndTitle, $iDialog_Width, $iMsg_Height, $iHpos, $iVPos, $iStyle, $iExtStyle, $parent)
   GUICtrlCreateLabel("Select which elements you want to include in the list", 10, 10)
   Local $idServicesChk = GUICtrlCreateCheckbox("Services", 10, 30)
   Local $aRect = ControlGetPos($strWndTitle, "", $idServicesChk)
   Local $idDriversChk = GUICtrlCreateCheckbox("Drivers", $aRect[0]+$aRect[2]+10, 30)
   $aRect = ControlGetPos($strWndTitle, "", $idDriversChk)
   Local $idTasksChk = GUICtrlCreateCheckbox("Tasks", $aRect[0]+$aRect[2]+10, 30)
   $aRect = ControlGetPos($strWndTitle, "", $idTasksChk)
   Local $idFeaturesChk = GUICtrlCreateCheckbox("Features", $aRect[0]+$aRect[2]+10, 30)
   Local $idOK = GUICtrlCreateButton("OK", $iDialog_Width/2 - 60, 70, 50, 20)
   Local $idCancel = GUICtrlCreateButton("Cancel", $iDialog_Width/2 + 10, 70, 50, 20)
   GUISetState(@SW_SHOW, $hMsgGUI)

   ; Set MessageLoop mode
   Local $iOrgMode = Opt('GUIOnEventMode', 0)
   Local $iRet_Value = 0x0000

   While 1
	  $aMsg = GUIGetMsg(1)
	  If $aMsg[1] == $hMsgGUI Then
		 Switch $aMsg[0]
			Case $GUI_EVENT_CLOSE, $idCancel
			   ExitLoop
			Case $idOK
			   If (GuiCtrlRead($idServicesChk) = $GUI_CHECKED) Then $iRet_Value = BitOR($iRet_Value, $hexServices)
			   If (GuiCtrlRead($idDriversChk) = $GUI_CHECKED) Then $iRet_Value = BitOR($iRet_Value, $hexDrivers)
			   If (GuiCtrlRead($idTasksChk) = $GUI_CHECKED) Then $iRet_Value = BitOR($iRet_Value, $hexTasks)
			   If (GuiCtrlRead($idFeaturesChk) = $GUI_CHECKED) Then $iRet_Value = BitOR($iRet_Value, $hexFeatures)
			   ExitLoop
		 EndSwitch
	  EndIf
   WEnd

   ; Reset original mode
   Opt('GUIOnEventMode', $iOrgMode)
   GUIDelete($hMsgGUI)

   Return $iRet_Value
EndFunc ;==> _CreateNewFile_Dlg

; #FUNCTION# =======================================================================================================================================================
; Name...........: _ShowWait_Dlg
; Description ...: Shows a modal window for long waits. Prevent user from clicking in the main window
; Syntax.........: _ShowWait_Dlg($hParentWnd, $message)
; Parameters ....: $hParentWnd - Handle of the main window
;                  $message - Message to show
; Requirement(s).:
; Return values .: Success - The handle of the modal window
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......: _HideWait_Dlg
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _ShowWait_Dlg($hParentWnd, $message)
   Local $iDialog_Width = 300
   Local $iMsg_Height = 100
   Local $iHpos = -1
   Local $iVPos = -1
   Local $iStyle = BitOR(0x80880000, 0x00C00000) ;$WS_OVERLAPPED
   Local $iExtStyle = -1
   Local $strWndTitle = "Wait..."

   WinGetHandle($strWndTitle)
   ; Exit if the window is already visible
   If (Not @error) Then Return 0

   Local $hWaitDlg = GUICreate($strWndTitle, $iDialog_Width, $iMsg_Height, $iHpos, $iVPos, $iStyle, $iExtStyle, $hParentWnd)
   GUICtrlCreateLabel($message, 20, 30, 250, 40)
   ; Disable main window
   GUISetState(@SW_SHOW, $hWaitDlg)
   GUISetState(@SW_DISABLE, $hParentWnd)
   Return $hWaitDlg
EndFunc ;==> _ShowWait_Dlg

; #FUNCTION# =======================================================================================================================================================
; Name...........: _HideWait_Dlg
; Description ...: Destroy the wait modal window
; Syntax.........: _HideWait_Dlg($hWaitDlg, $hParentWnd)
; Parameters ....: $hWaitDlg - Handle of the modal window to destroy
;                  $hParentWnd - Handle to the main window
; Requirement(s).:
; Return values .: Success - Void
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......: _ShowWait_Dlg
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _HideWait_Dlg($hWaitDlg, $hParentWnd)
   If ($hWaitDlg <> 0) Then
	  GUISetState(@SW_ENABLE, $hParentWnd)
	  GUIDelete($hWaitDlg)
   EndIf
EndFunc ;==> _HideWait_Dlg