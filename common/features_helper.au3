#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         Xatanú

 Script Function:
    Functions for interacting with "Windows Optional Features"

#ce ----------------------------------------------------------------------------

#cs ----------------------------------------------------------------------------
https://docs.microsoft.com/en-us/windows/desktop/wmisdk/querying-the-status-of-optional-features
https://social.technet.microsoft.com/wiki/contents/articles/575.script-to-determine-status-of-optional-features-using-win32-optionalfeature-wmi-class.aspx
https://docs.microsoft.com/es-es/windows/desktop/CIMWin32Prov/win32-optionalfeature
https://www.autoitscript.com/forum/topic/148390-wmi-query-how-to-get-data-from-object/
#ce ----------------------------------------------------------------------------

#include-once "logger_helper.au3"
#include-once "dialogs_helper.au3"

Global $aFeaturesList[0][3]

; #FUNCTION# =======================================================================================================================================================
; Name...........: _SetFeatureState
; Description ...: Enable or disable a feature
; Syntax.........: _SetFeatureState($featureid, $enable)
; Parameters ....: $featureid - The feature name (id)
;                  $enable - True/False Enable/Disable the feature
; Requirement(s).: Administrative rights on the target computer.
; Return values .: Success - True
;                  Failure - False
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _SetFeatureState($featureid, $enable)
   If ($enable) Then
	  $param = "/enable-feature /all"
   Else
	  $param = "/disable-feature"
   EndIf

   Local $iPID = Run("Dism /online " & $param & " /featurename:" & $featureid & " /English /NoRestart", "", @SW_HIDE, $STDOUT_CHILD)
   $res = ProcessWaitClose($iPID)
   $exitcode = @extended
   If ($res) Then
	  Local $sOutput = StdoutRead($iPID)
	  ;_WriteLineToLog($exitcode & " - " & $sOutput)
	  If (StringInStr($sOutput, "The operation completed successfully")) Then Return True
   EndIf
   Return False
EndFunc ;==> _SetFeatureState

; #FUNCTION# =======================================================================================================================================================
; Name...........: _EnableFeature
; Description ...: Enable a feature
; Syntax.........: _EnableFeature($featureid)
; Parameters ....: $featureid - The feature name (id)
; Requirement(s).: Administrative rights on the target computer.
; Return values .: Success - True
;                  Failure - False
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......: _SetFeatureState
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _EnableFeature($featureid)
   _WriteLineToLog("Please, wait while enabling feature: " & $featureid)
   $hWaitDlg = _ShowWait_Dlg($hGUI, "Please, wait while enabling the feature")
   $res = _SetFeatureState($featureid, True)
   _HideWait_Dlg($hWaitDlg, $hGUI)
   If ($res) Then
	  _WriteLineToLog("Feature enabled: " & $featureid)
   Else
	  _WriteLineToLog("Error enabling feature: " & $featureid, $LOG_ERROR)
   EndIf
   Return $res
EndFunc ;==> _EnableFeature

; #FUNCTION# =======================================================================================================================================================
; Name...........: _DisableFeature
; Description ...: Disable a feature
; Syntax.........: _DisableFeature($featureid)
; Parameters ....: $featureid - The feature name (id)
; Requirement(s).: Administrative rights on the target computer.
; Return values .: Success - True
;                  Failure - False
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......: _SetFeatureState
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _DisableFeature($featureid)
   _WriteLineToLog("Please, wait while disabling feature: " & $featureid)
   $hWaitDlg = _ShowWait_Dlg($hGUI, "Please, wait while disabling the feature")
   Local $res = _SetFeatureState($featureid, False)
   _HideWait_Dlg($hWaitDlg, $hGUI)
   If ($res) Then
	  _WriteLineToLog("Feature disabled: " & $featureid)
   Else
	  _WriteLineToLog("Error disabling feature: " & $featureid, $LOG_ERROR)
   EndIf
   Return $res
EndFunc ;==> _DisableFeature

; #FUNCTION# =======================================================================================================================================================
; Name...........: _GetFeatureStatus
; Description ...: Get the status of a Feature
; Syntax.........: _GetFeatureStatus($featureid)
; Parameters ....: $featureid - The feature name (id)
; Requirement(s).: Administrative rights on the target computer.
; Return values .: Success - Feature Status [Enabled (1), Disabled (2), Absent (3), Unknown (4)]
;                  Failure - -1
;                            Sets @error
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......: _EnumAllFeatures, _ResetFeaturesCache, _GetFeatureInfo
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _GetFeatureStatus($featureid)
   If (UBound($aFeaturesList) == 0) Then
	  _EnumAllFeatures()
   EndIf
   Local $iIndex = _ArraySearch($aFeaturesList, $featureid)
   If ($iIndex == -1) Then
	  SetError(1)
	  Return -1
   Else
	  Return $aFeaturesList[$iIndex][1]
   EndIf
EndFunc ;==> _GetFeatureStatus

; #FUNCTION# =======================================================================================================================================================
; Name...........: _DisplayIsFeatureEnabled
; Description ...: Return the literal to show for the Feature status
; Syntax.........: _DisplayIsFeatureEnabled($feature_is_enabled)
; Parameters ....: $feature_is_enabled - True/False Feature Enabled/Disabled
; Requirement(s).:
; Return values .: Success - "Enabled"
;                            "Disabled"
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......: _IsFeatureEnabled
; Link ..........:
; Example .......: _DisplayIsFeatureEnabled(True) will return "Enabled"
; ==================================================================================================================================================================
Func _DisplayIsFeatureEnabled($feature_is_enabled)
   If ($feature_is_enabled) Then
	  Return "Enabled"
   Else
	  Return "Disabled"
   EndIf
EndFunc ;==> _DisplayIsFeatureEnabled

; #FUNCTION# =======================================================================================================================================================
; Name...........: _IsFeatureEnabled
; Description ...: Check if the literal status of a Feature correponds to a Enabled status
; Syntax.........: _IsFeatureEnabled($strFeatureIsEnabled)
; Parameters ....: $strFeatureIsEnabled - Literal with the Feature status ["Enabled", "Disabled", "Absent", "Unknown"]
; Requirement(s).:
; Return values .: Success - True - Feature is enabled
;                            False - Feature is disabled
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......: _TranslateInstallationStatus
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _IsFeatureEnabled($strFeatureIsEnabled)
   If ($strFeatureIsEnabled == "Enabled") Then Return True
   Return False
EndFunc ;==> _IsFeatureEnabled

; #FUNCTION# =======================================================================================================================================================
; Name...........: _TranslateInstallationStatus
; Description ...: Translates the internal status of a feature into a literal
; Syntax.........: _TranslateInstallationStatus($iFeatureStatus)
; Parameters ....: $iFeatureStatus - Feature internal status [1, 2, 3, 4]
; Requirement(s).:
; Return values .: Success - Literal with the Feature status ["Enabled", "Disabled", "Absent", "Unknown"]
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......: _GetFeatureStatus
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _TranslateInstallationStatus($iFeatureStatus)
   Switch $iFeatureStatus
	  Case 1
		 Return "Enabled"
	  Case 2
		 Return "Disabled"
	  Case 3
		 Return "Absent"
	  Case 4
		 Return "Unknown"
   EndSwitch
EndFunc ;==> _TranslateInstallationStatus

; #FUNCTION# =======================================================================================================================================================
; Name...........: _GetFeatureInfo
; Description ...: Get information such as the installation status of a Feature or from all Features available
; Syntax.........: _GetFeatureInfo($featureid)
; Parameters ....: $featureid - Feature name (id) or "" to get information from all available Features in the machine
; Requirement(s).: Administrative rights
; Return values .: Success - Array with the following values for all the Features retrieved [name, installation_status, display_name]
;                  Failure - Empty array
;                            Sets @error
; Author ........: Xatanú
; Modified.......:
; Remarks .......: If invoked with "", it will cache the results of all the Features for future calls to _GetFeatureStatus
; Related .......: _EnumAllFeatures, _ResetFeaturesCache, _GetFeatureStatus
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _GetFeatureInfo($featureid)
   If ($featureid == "") Then
	  Global $aFeaturesList[0][3]
   Else
	  Local $aFeaturesList[0][3]
   EndIf

   Local $query = "select * from Win32_OptionalFeature"
   If ($featureid <> "") Then $query &= " where name = '" & $featureid & "'"

   Local $objWMI = ObjGet('winmgmts:root\cimv2')
   Local $objClass = $objWMI.ExecQuery($query)

   For $objItem In $objClass
	  $service_name = $objItem.Name
	  $service_status = _TranslateInstallationStatus($objItem.InstallState)
	  $service_display_name = $objItem.Caption
	  _ArrayAdd($aFeaturesList, $service_name&"|"&$service_status&"|"&$service_display_name)
   Next
   $objWMI = 0

   If ($ComErrorFound) Then
	  $ComErrorFound = 0
	  SetError(1)
   EndIf

   Return $aFeaturesList
EndFunc ;==> _GetFeatureInfo

; #FUNCTION# =======================================================================================================================================================
; Name...........: _EnumAllFeatures
; Description ...: Get information such as the installation status for all Features available
; Syntax.........: _EnumAllFeatures()
; Parameters ....:
; Requirement(s).: Administrative rights
; Return values .: Success - Array with the following values for all the Features retrieved [name, installation_status, display_name]
;                  Failure - Empty array
;                            Sets @error
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......: _ResetFeaturesCache, _GetFeatureInfo, _GetFeatureStatus
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _EnumAllFeatures()
   Return _GetFeatureInfo("")
EndFunc ;==> _EnumAllFeatures

; #FUNCTION# =======================================================================================================================================================
; Name...........: _ResetFeaturesCache
; Description ...: Reset the Features cache
; Syntax.........: _EnumAllFeatures()
; Parameters ....:
; Requirement(s).:
; Return values .: Success - Void
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......: _EnumAllFeatures, _GetFeatureInfo, _GetFeatureStatus
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _ResetFeaturesCache()
   Global $aFeaturesList[0][3]
EndFunc ;==> _ResetFeaturesCache