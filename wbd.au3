#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         Xatanú

 Script Version: 1.0
 Script Function:
    Disable Windows services, tasks and features

#ce ----------------------------------------------------------------------------

#include <GuiListView.au3>
#include <GUIConstants.au3>
#include <String.au3>

#include "common/logger_helper.au3"
#include "common/services_helper.au3"
#include "common/tasks_helper.au3"
#include "common/files_helper.au3"
#include "common/features_helper.au3"
#include "common/dialogs_helper.au3"


#requireadmin
#NoTrayIcon
Opt("GUIOnEventMode", 1)

Dim $nSortDir = 1 ; toggle for sort up/down
Dim $bSet = 0 ; detect the 1st call of _LVSort() within a sorting sequence
Dim $nCol = -1 ; preserve last column to detect 2nd click (for reverse direction)
Dim $window_width = 1200
Dim $window_height = 600
Dim $log_window_height = 80
Dim $separator_heigh = 10
Dim $buttons_height = 20
Local $strWindowTitle = "Windows Bloat disabler"
Local $PROFILE_EXTENSION = "*.pro"

$oMyError = ObjEvent("AutoIt.Error","_COMErrFunc")    ; Initialize a COM error handler

; Create window
Global $hGUI = GUICreate($strWindowTitle, $window_width, $window_height, -1, -1, BitOR($WS_MINIMIZEBOX, $WS_CAPTION, $WS_POPUP, $WS_SYSMENU))
; create an zero-width column between 'Column1' and 'Col2' just for sorting the checkboxes
Global $hListView = GUICtrlCreateListView("", 10, 10, $window_width-20, $window_height-$log_window_height-$separator_heigh*3-$buttons_height, -1, BitOR($LVS_EX_CHECKBOXES, $LVS_EX_HEADERDRAGDROP, $LVS_EX_FULLROWSELECT, $WS_EX_CLIENTEDGE, $LVS_EX_FLATSB))

; Create buttons
Local $idSaveProfile = GUICtrlCreateButton("Save Profile", $window_width/2-200, $window_height-$log_window_height-$separator_heigh-$buttons_height, -1, $buttons_height)
$aRect = ControlGetPos($strWindowTitle, "", $idSaveProfile)
Local $idLoadProfile = GUICtrlCreateButton("Load Profile", $aRect[0]+$aRect[2]+10, $window_height-$log_window_height-$separator_heigh-$buttons_height, -1, $buttons_height)
$aRect = ControlGetPos($strWindowTitle, "", $idLoadProfile)
Local $idEnableAll = GUICtrlCreateButton("Enable All", $aRect[0]+$aRect[2]+10, $window_height-$log_window_height-$separator_heigh-$buttons_height, -1, $buttons_height)
$aRect = ControlGetPos($strWindowTitle, "", $idEnableAll)
Local $idDisableAll = GUICtrlCreateButton("Disable All", $aRect[0]+$aRect[2]+10, $window_height-$log_window_height-$separator_heigh-$buttons_height, -1, $buttons_height)
$aRect = ControlGetPos($strWindowTitle, "", $idDisableAll)
Local $idRestoreDefaults = GUICtrlCreateButton("Restore Defaults", $aRect[0]+$aRect[2]+10, $window_height-$log_window_height-$separator_heigh-$buttons_height, -1, $buttons_height)
$aRect = ControlGetPos($strWindowTitle, "", $idRestoreDefaults)
Local $idRefresh = GUICtrlCreateButton("Refresh", $aRect[0]+$aRect[2]+10, $window_height-$log_window_height-$separator_heigh-$buttons_height, -1, $buttons_height)

; create log box
Global $Log_Edit = _GUICtrlRichEdit_Create($hGUI, "", 10, $window_height-$log_window_height, $window_width-20, $log_window_height-$separator_heigh, BitOR($ES_MULTILINE,$ES_AUTOVSCROLL,$ES_READONLY,$ES_WANTRETURN,$WS_VSCROLL))
;~ Global $Log_Edit = GUICtrlCreateEdit("", 10, $window_height-$log_window_height, $window_width-20, $log_window_height-$separator_heigh, BitOR($ES_AUTOVSCROLL,$ES_READONLY,$ES_WANTRETURN,$WS_VSCROLL))

; Add columns
_GUICtrlListView_AddColumn($hListView, "", 20)					; Checbox: service startup disabled? [is_disabled]
_GUICtrlListView_AddColumn($hListView, "", 0)					; Reserved for the checkbox status
_GUICtrlListView_AddColumn($hListView, "ID", 450)				; Service ID [id]
_GUICtrlListView_AddColumn($hListView, "Display Name", 400)		; Display Name [display_name]
_GUICtrlListView_AddColumn($hListView, "Status", 60)			; Status [status]
_GUICtrlListView_AddColumn($hListView, "Type", 60)				; Type [type]
_GUICtrlListView_AddColumn($hListView, "Startup Type", 80)		; Startup type [startup_type]
_GUICtrlListView_AddColumn($hListView, "Stock Startup", 80)		; Stock Startup type [stock_startup]
_GUICtrlListView_AddColumn($hListView, "", 0)					; Reserved for the warn level [warn_level]

; Read items from file
$aServicesList = _ReadComponentsFile()
If (@error) Then
   $aServicesList = _CreateNewComponentsList()
EndIf

_AddItems($aServicesList)

; Register events
GUISetOnEvent($GUI_EVENT_CLOSE, "_Terminate")
GUICtrlSetOnEvent($hListView, "_Setsort") ; clicking the column header will start the sort
GUICtrlRegisterListViewSort($hListView, "_LVSort") ; Register the function "SortLV" as the sorting callback
_Register_WM_NOTIFY() ; use WM_NOTIFY to detect change of checkbox state and update hidden column
GUIRegisterMsg($WM_COMMAND, "_WM_COMMAND") ; check button clicks
;~ GUIRegisterMsg($WM_SIZE, "_WM_SIZE")

; shrink the (hidden) sorting column #1 to 0 pt width
_GUICtrlListView_SetColumnWidth($hListView, 1, 0)

; Show window
GUISetState()

While 1
  Sleep(10)
WEnd

;-------------------------------------------------------------------------------------------------
;-------------------------------------------------------------------------------------------------
;-------------------------------------- ASORTED FUNCTIONS ----------------------------------------
;-------------------------------------------------------------------------------------------------
;-------------------------------------------------------------------------------------------------

; #FUNCTION# =======================================================================================================================================================
; Name...........: _COMErrFunc
; Description ...: Catchs all the COM errors to prevent the message box from appearing when there is any error
; Syntax.........: _COMErrFunc($oError)
; Parameters ....: $oError - COM error
; Requirement(s).:
; Return values .:
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _COMErrFunc($oError)
;~    ConsoleWrite(@ScriptName & " (" & $oError.scriptline & ") : ==> COM Error intercepted !" & @CRLF & _
;~ 		 @TAB & "err.number is: " & @TAB & @TAB & "0x" & Hex($oError.number) & @CRLF & _
;~ 		 @TAB & "err.windescription:" & @TAB & $oError.windescription & @CRLF & _
;~ 		 @TAB & "err.description is: " & @TAB & $oError.description & @CRLF & _
;~ 		 @TAB & "err.source is: " & @TAB & @TAB & $oError.source & @CRLF & _
;~ 		 @TAB & "err.helpfile is: " & @TAB & $oError.helpfile & @CRLF & _
;~ 		 @TAB & "err.helpcontext is: " & @TAB & $oError.helpcontext & @CRLF & _
;~ 		 @TAB & "err.lastdllerror is: " & @TAB & $oError.lastdllerror & @CRLF & _
;~ 		 @TAB & "err.scriptline is: " & @TAB & $oError.scriptline & @CRLF & _
;~ 		 @TAB & "err.retcode is: " & @TAB & "0x" & Hex($oError.retcode) & @CRLF & @CRLF)
   SetError(1, 0, 0)
   Return 0
EndFunc ;==> _COMErrFunc

; #FUNCTION# =======================================================================================================================================================
; Name...........: _CreateNewComponentsList
; Description ...: Create a new components list asking the user which components to include
; Syntax.........: _CreateNewComponentsList()
; Parameters ....:
; Requirement(s).:
; Return values .: Success - Array with the information from all the components
;                  Failure - Empty array
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......: _CreateNewFile_Dlg, _FormatComponentsArrayList
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _CreateNewComponentsList()
   Local $aServicesList[0]

   $iRetValue = _CreateNewFile_Dlg($hGUI)

   ; Show wait dlg
   $hWaitDlg = _ShowWait_Dlg($hGUI, "Please, wait while retrieving the information from the system")

   ; Services
   If (BitAND($iRetValue, $hexServices)) Then
	  _ArrayAdd($aServicesList, _FormatComponentsArrayList(_EnumAllServices($SERVICE_WIN32), "Service"))
   EndIf

   ; Drivers
   If (BitAND($iRetValue, $hexDrivers)) Then
	  _ArrayAdd($aServicesList, _FormatComponentsArrayList(_EnumAllServices($SERVICE_DRIVER), "Driver"))
   EndIf

   ; Tasks
   If (BitAND($iRetValue, $hexTasks)) Then
	  ; TODO - enum all tasks
	  _ArrayAdd($aServicesList, _FormatComponentsArrayList(_EnumAllTasks(), "Task"))
   EndIf

   ; Features
   If (BitAND($iRetValue, $hexFeatures)) Then
	  _ArrayAdd($aServicesList, _FormatComponentsArrayList(_EnumAllFeatures(), "Feature"))
   EndIf

   ; Save the list to a file
   _CreateNewComponentsFile($aServicesList)

   ; Hide wait dlg
   _HideWait_Dlg($hWaitDlg, $hGUI)

   Return $aServicesList
EndFunc ;==> _CreateNewComponentsList


;-------------------------------------------------------------------------------------------------
;-------------------------------------------------------------------------------------------------
;------------------------------------ LIST VIEW FUNCTIONS ----------------------------------------
;-------------------------------------------------------------------------------------------------
;-------------------------------------------------------------------------------------------------
; #FUNCTION# =======================================================================================================================================================
; Name...........: _Setsort
; Description ...: Prepare for sorting the column in the ListView control
; Syntax.........: _Setsort()
; Parameters ....:
; Requirement(s).:
; Return values .:
; Author ........: ?
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _Setsort()
   $bSet = 0 ; initialize for new sorting

   ; bells and whistles (I):
   ; mark the sorted column with a grey rectangle
   GUICtrlSendMsg($hListView, $LVM_SETSELECTEDCOLUMN, GUICtrlGetState($hListView), 0)
   DllCall("user32.dll", "int", "InvalidateRect", "hwnd", GUICtrlGetHandle($hListView), "int", 0, "int", 1)

   ; bells and whistles (II):
   ; create an arrow in the listview header
   Local $iFormat
   Local Const $hHeader = _GUICtrlListView_GetHeader($hListView)
   ; clear existing arrows
   For $x = 0 To  _GUICtrlHeader_GetItemCount($hHeader) - 1
	  $iFormat = _GUICtrlHeader_GetItemFormat($hHeader, $x)
	  If BitAND($iFormat, $HDF_SORTDOWN) Then
		 _GUICtrlHeader_SetItemFormat($hHeader, $x, BitXOR($iFormat, $HDF_SORTDOWN))
	  ElseIf BitAND($iFormat, $HDF_SORTUP) Then
		 _GUICtrlHeader_SetItemFormat($hHeader, $x, BitXOR($iFormat, $HDF_SORTUP))
	  EndIf
   Next
   ; set arrow in current column
   Local $nColumn = GUICtrlGetState($hListView)
   $iFormat = _GUICtrlHeader_GetItemFormat($hHeader, $nColumn)
   If $nSortDir == 1 And $nCol == $nColumn Then ; ascending
	  _GUICtrlHeader_SetItemFormat($hHeader, $nColumn, BitOR($iFormat, $HDF_SORTUP))
   Else ; descending
	  _GUICtrlHeader_SetItemFormat($hHeader, $nColumn, BitOR($iFormat, $HDF_SORTDOWN))
   EndIf
EndFunc   ;==>_Setsort


;===============================================================================
;
; Function Name..: _LVSort
; Description....: Sort listview columns - checkboxes, text, dates.
; Parameters.....: $hWnd    - The controlID of the listview control for which the callback function is used.
;                  $nItem1  - The lParam value of the first item (by default the item controlID).
;                  $nItem2  - The lParam value of the second item (by default the item controlID).
;                  $nColumn - The column that was clicked for sorting (the first column number is 0).
; Requirements...: use as callback function for GUICtrlRegisterListViewSort()
; Return Values..: -1       - 1st item should precede the 2nd.
;                   0       - No Change.
;                   1       - 1st item should follow the 2nd.
; Author.........: see example of function reference GUICtrlRegisterListViewSort()
; Modified.......: ogeiz: sort checkboxes, optimized direction checks, stable sort (revert sequence of equal values
;                  with direction change to permit sort sequences: least important column to most important column)
;
;===============================================================================
Func _LVSort($hWnd, $nItem1, $nItem2, $nColumn)
   Local $val1, $val2
   ; Switch the sorting direction
   If Not $bSet Then
	  If $nColumn = $nCol Then
		 $nSortDir = -$nSortDir
	  Else
		 $nSortDir = 1
	  EndIf
	  $bSet = 1
   EndIf
   $nCol = $nColumn

   ; sort depends on content of column (column starts with 0)
   Switch $nColumn
	  Case 0, 1 ; checkboxes (column 0) => use always column 1 to sort
		 $val1 = _GetSubItemText($hListView, $nItem1, 1) ; use '[x]'==0 to sort at top and '[ ]'==1 at bottom
		 $val2 = _GetSubItemText($hListView, $nItem2, 1)
	  Case Else ; text
		 $val1 = _GetSubItemText($hListView, $nItem1, $nColumn)
		 $val2 = _GetSubItemText($hListView, $nItem2, $nColumn)
;~ 	  Case 3 ; dates - reorder from dd.mm.yyyy -> yyyymmdd, then sort by value
;~ 		 $val1 = _GetSubItemText($hListView, $nItem1, $nColumn)
;~ 		 $val2 = _GetSubItemText($hListView, $nItem2, $nColumn)
;~ 		 $val1 = StringRight($val1, 4) & StringMid($val1, 4, 2) & StringLeft($val1, 2)
;~ 		 $val2 = StringRight($val2, 4) & StringMid($val2, 4, 2) & StringLeft($val2, 2)
   EndSwitch

   If $val1 < $val2 Or ($val1 == $val2 And $nItem1 < $nItem2) Then
	  Return -$nSortDir ; Put item2 before item1
   Else
	  Return $nSortDir ; Put item2 behind item1
   EndIf
EndFunc   ;==>_LVSort

; #FUNCTION# =======================================================================================================================================================
; Name...........: _GetSubItemText
; Description ...: Retrieve the text of a listview item in a specified column
; Syntax.........: _GetSubItemText($nCtrlID, $nItemID, $nColumn)
; Parameters ....: $nCtrlID - Control ID
;                  $nItemID - Row
;                  $nColumn - Column
; Requirement(s).:
; Return values .: Success - Text from the cell
; Author.........: see example of function reference GUICtrlRegisterListViewSort()
;===============================================================================
Func _GetSubItemText($nCtrlID, $nItemID, $nColumn)
   Local $stLvfi = DllStructCreate("uint;ptr;int;int[2];int")
   Local $nIndex, $stBuffer, $stLvi, $sItemText

   DllStructSetData($stLvfi, 1, $LVFI_PARAM)
   DllStructSetData($stLvfi, 3, $nItemID)

   $stBuffer = DllStructCreate("char[260]")

   $nIndex = GUICtrlSendMsg($nCtrlID, $LVM_FINDITEM, -1, DllStructGetPtr($stLvfi));

   $stLvi = DllStructCreate("uint;int;int;uint;uint;ptr;int;int;int;int")

   DllStructSetData($stLvi, 1, $LVIF_TEXT)
   DllStructSetData($stLvi, 2, $nIndex)
   DllStructSetData($stLvi, 3, $nColumn)
   DllStructSetData($stLvi, 6, DllStructGetPtr($stBuffer))
   DllStructSetData($stLvi, 7, 260)

   GUICtrlSendMsg($nCtrlID, $LVM_GETITEMA, 0, DllStructGetPtr($stLvi));

   $sItemText = DllStructGetData($stBuffer, 1)

   $stLvi = 0
   $stLvfi = 0
   $stBuffer = 0

   Return $sItemText
EndFunc   ;==>_GetSubItemText

; #FUNCTION# =======================================================================================================================================================
; Name...........: _AddItems
; Description ...: Add all the Components to the ListView control
; Syntax.........: _AddItems($aServicesList)
; Parameters ....: $aServicesList - Components list array as loaded from file
; Requirement(s).:
; Return values .:
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......: _CreateItemObj, _AddItem
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _AddItems($aServicesList)
   ; Add Items
   If (IsArray($aServicesList)) Then
	  $hWaitDlg = _ShowWait_Dlg($hGUI, "Please, wait while checking services status")
	  For $i = 0 To UBound($aServicesList) -1
		 Dim $aCurService = StringSplit($aServicesList[$i],";")
		 If (IsArray($aCurService)) Then
			$item = _CreateItemObj($aCurService)
			_AddItem($item)
		 EndIf
	  Next
	  _HideWait_Dlg($hWaitDlg, $hGUI)
   EndIf
EndFunc   ;==>_AddItems

; #FUNCTION# =======================================================================================================================================================
; Name...........: _AddItem
; Description ...: Append a new row to the ListView control
; Syntax.........: _AddItem($item)
; Parameters ....: $item - "Scripting.Dictionary" object
; Requirement(s).:
; Return values .:
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......: _CreateItemObj, _SetListViewItemColor
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _AddItem($item)
   Local $item_str
   Local $checked

   $type = $item.Item("type")
   $display_item_status = $item.Item("status")
   $display_item_startup_type = $item.Item("startup_type")

   If ($item.Item("is_disabled")) Then
	  $item_str = "|1"
	  $checked = False
   Else
	  $item_str = "|0"
	  $checked = True
   EndIf

   $item_str &= "|" & $item.Item("id")
   $item_str &= "|" & $item.Item("display_name")
   $item_str &= "|" & $display_item_status
   $item_str &= "|" & $item.Item("type")
   $item_str &= "|" & $display_item_startup_type
   $item_str &= "|" & $item.Item("stock_startup")
   $item_str &= "|" & $item.Item("warn_level")

   $lvi1 = GUICtrlCreateListViewItem($item_str, $hListView)
   If ($checked) Then
	  _GUICtrlListView_SetItemChecked($hListView, _GUICtrlListView_GetItemCount($hListView)-1, True)
   EndIf

   ; Set background color
   _SetListViewItemColor(_GUICtrlListView_GetItemCount($hListView)-1)

   If ($item.Exists("not_found")) Then
	  _RemoveCheckbox($hListView, _GUICtrlListView_GetItemCount($hListView)-1, $lvi1)
   EndIf
EndFunc   ;==>_AddItem

; #FUNCTION# =======================================================================================================================================================
; Name...........: _SetListViewItemColor
; Description ...: Change the color of a ListView row
; Syntax.........: _SetListViewItemColor($iItemIndex, [$refresh])
; Parameters ....: $iItemIndex - row
;                  $refresh - If $refresh is True and warn_level is 1 or 2, it does nothing in order to preserve the previous color
; Requirement(s).:
; Return values .:
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _SetListViewItemColor($iItemIndex, $refresh=False)
   Local $color_warn1 = 0xfaf554
   Local $color_warn2 = 0xffb164
   Local $color_manual_stopped = 0xe6e6e6
   Local $color_changed = 0x8080FF
   Local $default_color = 0xFFFFFF

   Local $new_color = $default_color

   $ctrlID = _GUICtrlListView_GetItemParam($hListView, $iItemIndex)

   $warn_level = _GUICtrlListView_GetItemText($hListView, $iItemIndex, 8)
   If ($refresh And ($warn_level == 1 Or $warn_level == 2)) Then
	  Return ; do nothing
   Else
	  $item_checked = _GUICtrlListView_GetItemChecked($hListView, $iItemIndex)
	  $item_status = _GUICtrlListView_GetItemText($hListView, $iItemIndex, 4)
	  $item_type = _GUICtrlListView_GetItemText($hListView, $iItemIndex, 5)
	  $stock_startup = _GUICtrlListView_GetItemText($hListView, $iItemIndex, 7)
	  If ($warn_level == 0 And ($item_type == "Service" Or $item_type == "Driver") And $item_checked And _IsServiceStoppedStr($item_status)) Then $warn_level = 3
	  If ($warn_level == 0 And ($item_checked == ($stock_startup == "Disabled" Or $stock_startup == "Absent"))) Then $warn_level = 4
	  Switch $warn_level
		 Case 0
			$new_color = $default_color
		 Case 1
			$new_color = $color_warn1
		 Case 2
			$new_color = $color_warn2
		 Case 3
			$new_color = $color_manual_stopped
		 Case 4
			$new_color = $color_changed
	  EndSwitch
	  GUICtrlSetBkColor($ctrlID, $new_color)
   EndIf
EndFunc   ;==>_SetListViewItemColor

; #FUNCTION# =======================================================================================================================================================
; Name...........: _RemoveCheckbox
; Description ...: Remove the checkbox of the selected row (intended for unavailable components in the machine)
; Syntax.........: _RemoveCheckbox($LVM, $nIndex, $objId)
; Parameters ....: $LVM - ListView handle
;                  $nIndex - Row index of the ListView
;                  $objId - ID of the row as created by GUICtrlCreateListViewItem
; Requirement(s).:
; Return values .:
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _RemoveCheckbox($LVM, $nIndex, $objId)
   _GUICtrlListView_SetItemText($LVM, $nIndex, "-1", 1)
   GUICtrlSetColor($objId, 0xaaaaaa)
   _GUICtrlListView_SetItemState($LVM, $nIndex, 0, $LVIS_STATEIMAGEMASK)
   _WinAPI_RedrawWindow($LVM)
EndFunc   ;==>_RemoveCheckbox

; #FUNCTION# =======================================================================================================================================================
; Name...........: _CreateItemObj
; Description ...: Creates a dictionary object with all the component information retrieved from the system
; Syntax.........: _CreateItemObj($aCurService)
; Parameters ....: $aCurService - Array with the identifier and basic information from the component
; Requirement(s).:
; Return values .: Success - Dictionary obj with all the component information
;                  Failure - False
;                            Sets @error
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _CreateItemObj($aCurService)
   If (IsArray($aCurService)) Then
	  $item_type = _StringTitleCase($aCurService[1])
	  $itemid = $aCurService[2]
	  $stock_startup = _StringTitleCase($aCurService[4])
	  $warn_level = $aCurService[5]
	  If ($warn_level < 1 Or $warn_level > 2) Then $warn_level = 0

	  $item = ObjCreate("Scripting.Dictionary")
	  $item.Add("type", $item_type)
	  $item.Add("id", $itemid)
	  $item.Add("stock_startup", $stock_startup)
	  ; Internal states
	  $item.Add("warn_level", $warn_level)
	  Switch $item_type
		 Case "Service", "Driver"
			$item_status = _Service_QueryStatus($itemid)
			$display_name = StringStripWS($aCurService[3], $STR_STRIPLEADING + $STR_STRIPTRAILING)
			If ($display_name == "") Then $display_name = _Service_QueryDisplayName($itemid)
			If (@error) Then
			   $item.Add("not_found", True)
			Else
			   $item_startup_type = _Service_QueryStartType($itemid)
			   ; Display texts
			   $item.Add("display_name", $display_name)
			   $item.Add("status", _DisplayServiceStatus($item_status))
			   $item.Add("startup_type", _DisplayServiceStartType($item_startup_type))
			   ; Internal states
			   $item.Add("is_stopped", _IsServiceStopped($item_status))
			   $item.Add("is_disabled", _IsServiceDisabled($item_startup_type))
			EndIf
		 Case "Task"
			$display_name = $aCurService[3]
			$task_exists = _TaskExists($itemid)
			If (@error Or not $task_exists) Then
			   $item.Add("not_found", True)
			Else
			   $item_status = _IsTaskRunning($itemid)
			   $item_startup_type = _TaskIsEnabled($itemid)
			   ; Display texts
			   $item.Add("display_name", $display_name)
			   $item.Add("status", _DisplayTaskRunning($item_status))
			   $item.Add("startup_type", _DisplayIsTaskEnabled($item_startup_type))
			   ; Internal states
			   $item.Add("is_stopped", Not $item_status)
			   $item.Add("is_disabled", Not $item_startup_type)
			EndIf
		 Case "Feature"
			$display_name = $aCurService[3]
			If (@error) Then
			   $item.Add("not_found", True)
			Else
			   $item_status = _GetFeatureStatus($itemid)
			   $item_startup_type = $item_status
			   ; Display texts
			   $item.Add("display_name", $display_name)
			   $item.Add("status", $item_status)
			   $item.Add("startup_type", $item_startup_type)
			   ; Internal states
			   $item.Add("is_stopped", Not _IsFeatureEnabled($item_status))
			   $item.Add("is_disabled", Not _IsFeatureEnabled($item_startup_type))
			EndIf
	  EndSwitch
	  Return $item
   EndIf

   SetError(1)
   Return False
EndFunc   ;==>_CreateItemObj

; #FUNCTION# =======================================================================================================================================================
; Name...........: _DisableItem
; Description ...: Disable the component based on its type
; Syntax.........: _DisableItem($itemid, $item_type)
; Parameters ....: $itemid - Array with the identifier and basic information from the component
;                  $item_type - Type of the component [Service, Driver, Task, Feature]
; Requirement(s).:
; Return values .: Success - True
;                  Failure - False
;                            Sets @error
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _DisableItem($itemid, $item_type)
   Local $res = False
   Switch $item_type
	  Case "Service", "Driver"
		 $res = _DisableService($itemid, True)
	  Case "Task"
		 $res = _DisableTask($itemid)
	  Case "Feature"
		 $res = _DisableFeature($itemid)
   EndSwitch
   Return $res
EndFunc   ;==>_DisableItem

; #FUNCTION# =======================================================================================================================================================
; Name...........: _EnableItem
; Description ...: Enable the component based on its type
; Syntax.........: _EnableItem($itemid, $item_type, $stock_startup)
; Parameters ....: $itemid - Array with the identifier and basic information from the component
;                  $item_type - Type of the component [Service, Driver, Task, Feature]
;                  $stock_startup - Stock startup (only for services)
; Requirement(s).:
; Return values .: Success - True
;                  Failure - False
;                            Sets @error
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _EnableItem($itemid, $item_type, $stock_startup)
   Switch $item_type
	  Case "Service", "Driver"
		 $res = _EnableService($itemid, $stock_startup)
	  Case "Task"
		 $res = _EnableTask($itemid)
	  Case "Feature"
		 $res = _EnableFeature($itemid)
   EndSwitch
   Return $res
EndFunc   ;==>_EnableItem

; #FUNCTION# =======================================================================================================================================================
; Name...........: _RefreshItem
; Description ...: Refresh the listview row with the new status after disabling or enabling a service
; Syntax.........: _RefreshItem($hWndListView, $iItem, $itemid, $item_type)
; Parameters ....: $hWndListView - Handle of the ListView control
;                  $iItem - Row index of the element to refresh
;                  $itemid - Name (identifier) of the component
;                  $item_type - Type of the component [Service, Driver, Task, Feature]
; Requirement(s).:
; Return values .:
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _RefreshItem($hWndListView, $iItem, $itemid, $item_type)
   Switch $item_type
	  Case "Service", "Driver"
		 $status_txt = _DisplayServiceStatus(_Service_QueryStatus($itemid))
		 $startup_type_txt = _DisplayServiceStartType(_Service_QueryStartType($itemid))
		 _GUICtrlListView_SetItemText($hWndListView, $iItem, $status_txt, 4)
		 _GUICtrlListView_SetItemText($hWndListView, $iItem, $startup_type_txt, 6)
	  Case "Task"
		 $status_txt = _DisplayTaskRunning(_IsTaskRunning($itemid))
		 $startup_type_txt = _DisplayIsTaskEnabled(_TaskIsEnabled($itemid))
		 _GUICtrlListView_SetItemText($hWndListView, $iItem, $status_txt, 4)
		 _GUICtrlListView_SetItemText($hWndListView, $iItem, $startup_type_txt, 6)
	  Case "Feature"
		 $aFeatureInfo = _GetFeatureInfo($itemid)
		 If (IsArray($aFeatureInfo) And UBound($aFeatureInfo) > 0) Then
			$status_txt = $aFeatureInfo[0][1]
			$startup_type_txt = $status_txt
			_GUICtrlListView_SetItemText($hWndListView, $iItem, $status_txt, 4)
			_GUICtrlListView_SetItemText($hWndListView, $iItem, $startup_type_txt, 6)
		 EndIf
   EndSwitch
   _SetListViewItemColor($iItem, True)
EndFunc   ;==>_RefreshItem


;-------------------------------------------------------------------------------------------------
;-------------------------------------------------------------------------------------------------
;---------------------------------------  BUTTON HANDLERS ----------------------------------------
;-------------------------------------------------------------------------------------------------
;-------------------------------------------------------------------------------------------------
; #FUNCTION# =======================================================================================================================================================
; Name...........: _EnableAll
; Description ...: Enable all the components in the ListView control
; Syntax.........: _EnableAll()
; Parameters ....:
; Requirement(s).:
; Return values .:
; Author ........: Xatanú
; Modified.......:
; Remarks .......: Display a modal wait dialog during the operation
; Related .......:
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _EnableAll()
   _WriteLineToLog("Enabling all items")
   $iNumElems = ControlListView("", "", $hListView, "GetItemCount")
   $hWaitDlg = _ShowWait_Dlg($hGUI, "Please, wait while enabling all elements")
   For $iItem = 0 To $iNumElems - 1
	  If (Not _GUICtrlListView_GetItemChecked($hListView, $iItem)) Then _GUICtrlListView_SetItemChecked($hListView, $iItem, True)
   Next
   _HideWait_Dlg($hWaitDlg, $hGUI)
EndFunc   ;==>_EnableAll

; #FUNCTION# =======================================================================================================================================================
; Name...........: _DisableAll
; Description ...: Disable all the components in the ListView control
; Syntax.........: _DisableAll()
; Parameters ....:
; Requirement(s).:
; Return values .:
; Author ........: Xatanú
; Modified.......:
; Remarks .......: Display a modal wait dialog during the operation
; Related .......:
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _DisableAll()
   _WriteLineToLog("Disabling all items")
   $iNumElems = ControlListView("", "", $hListView, "GetItemCount")
   $hWaitDlg = _ShowWait_Dlg($hGUI, "Please, wait while disabling all elements")
   For $iItem = 0 To $iNumElems - 1
	  If (_GUICtrlListView_GetItemChecked($hListView, $iItem)) Then _GUICtrlListView_SetItemChecked($hListView, $iItem, False)
   Next
   _HideWait_Dlg($hWaitDlg, $hGUI)
EndFunc   ;==>_DisableAll

; #FUNCTION# =======================================================================================================================================================
; Name...........: _RefreshAll
; Description ...: Refresh the entire list of components
; Syntax.........: _RefreshAll()
; Parameters ....:
; Requirement(s).:
; Return values .:
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _RefreshAll()
   _Register_WM_NOTIFY(False)
   _GUICtrlListView_DeleteAllItems($hListView)
   ; Reload cached list of feature status
   _EnumAllFeatures()
   _AddItems($aServicesList)
   _Register_WM_NOTIFY()
   _WriteLineToLog("List refreshed")
EndFunc   ;==>_RefreshAll

; #FUNCTION# =======================================================================================================================================================
; Name...........: _RestoreDefaults
; Description ...: Restore the default stock startup status of all the components in the ListView control
; Syntax.........: _RestoreDefaults()
; Parameters ....:
; Requirement(s).:
; Return values .:
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _RestoreDefaults()
   _WriteLineToLog("Restoring default StartUp states for all items")
   $iNumElems = ControlListView("", "", $hListView, "GetItemCount")
   $hWaitDlg = _ShowWait_Dlg($hGUI, "Please, wait while restoring stock startup states")
   For $iItem = 0 To $iNumElems - 1
	  $item_checked = _GUICtrlListView_GetItemChecked($hListView, $iItem)
	  $item_id = _GUICtrlListView_GetItemText($hListView, $iItem, 2)
	  $stock_startup = _GUICtrlListView_GetItemText($hListView, $iItem, 7)
	  If ($stock_startup == "Disabled" Or $stock_startup == "Absent") Then
		 If ($item_checked) Then _GUICtrlListView_SetItemChecked($hListView, $iItem, False)
	  ElseIf (Not $item_checked) Then
		 _GUICtrlListView_SetItemChecked($hListView, $iItem, True)
	  EndIf
   Next
   _HideWait_Dlg($hWaitDlg, $hGUI)
EndFunc   ;==>_RestoreDefaults

; #FUNCTION# =======================================================================================================================================================
; Name...........: _SaveProfile
; Description ...: Save a profile with the current status of all components as shown in the ListView
; Syntax.........: _SaveProfile()
; Parameters ....:
; Requirement(s).:
; Return values .:
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _SaveProfile()
   Local $file_path = FileSaveDialog("Save profile", "", "Profiles (" & $PROFILE_EXTENSION & ")", $FD_PATHMUSTEXIST + $FD_PROMPTOVERWRITE)
   If (Not @error) Then
	  Local $aServicesList[0]
	  For $iItem = 0 To ControlListView("", "", $hListView, "GetItemCount") - 1
		 $item_type = _GUICtrlListView_GetItemText($hListView, $iItem, 5)
		 $item_id = _GUICtrlListView_GetItemText($hListView, $iItem, 2)
		 $item_display = _GUICtrlListView_GetItemText($hListView, $iItem, 3)
		 $stock_startup = _GUICtrlListView_GetItemText($hListView, $iItem, 7)
		 $warning_level = _GUICtrlListView_GetItemText($hListView, $iItem, 8)
		 If (_GUICtrlListView_GetItemChecked($hListView, $iItem)) Then
			$item_checked = 1
		 Else
			$item_checked = 0
		 EndIf
		 _ArrayAdd($aServicesList, $item_type & ";" & $item_id & ";" & $item_display & ";" & $stock_startup & ";" & $warning_level & ";" & $item_checked)
	  Next
	  If (_CreateNewComponentsFile($aServicesList, $file_path)) Then
		 _WriteLineToLog("Profile saved to " & $file_path)
	  EndIf
   EndIf
EndFunc   ;==>_SaveProfile

; #FUNCTION# =======================================================================================================================================================
; Name...........: _LoadProfile
; Description ...: Load a previously saved profile and restore the components status to the saved values
; Syntax.........: _LoadProfile()
; Parameters ....:
; Requirement(s).:
; Return values .:
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _LoadProfile()
   _Register_WM_NOTIFY(False)
   ; Display an open dialog to select a list of file(s).
   Local $file_path = FileOpenDialog("Open profile", "", "Profiles (" & $PROFILE_EXTENSION & ")", $FD_FILEMUSTEXIST)
   _Register_WM_NOTIFY()
   If (Not @error) Then
	  _WriteLineToLog("Loading profile from " & $file_path)
	  $aServicesList = _ReadComponentsFile($file_path)
	  If (IsArray($aServicesList) And UBound($aServicesList) > 0) Then
			$hWaitDlg = _ShowWait_Dlg($hGUI, "Please, wait while restoring profile")
			_Register_WM_NOTIFY(False)
			_GUICtrlListView_DeleteAllItems($hListView)
			; Reload cached list of feature status
			_EnumAllFeatures()
			_AddItems($aServicesList)
			_Register_WM_NOTIFY()

			For $iItem = 0 To UBound($aServicesList) - 1
			   Local $aCurService = StringSplit($aServicesList[$iItem],";")
			   If (IsArray($aCurService) And UBound($aCurService) >= 7) Then
				  $item_checked = _GUICtrlListView_GetItemChecked($hListView, $iItem)
				  $item_profile_enabled = ($aCurService[6] == "1")
				  If ($item_profile_enabled <> $item_checked) Then _GUICtrlListView_SetItemChecked($hListView, $iItem, Not $item_checked)
			   Else
				  _WriteLineToLog("Profile line is invalid: " & $aServicesList[$iItem], $LOG_ERROR)
			   EndIf
			Next
			_HideWait_Dlg($hWaitDlg, $hGUI)
	  EndIf
   EndIf
EndFunc   ;==>_LoadProfile



;-------------------------------------------------------------------------------------------------
;-------------------------------------------------------------------------------------------------
;----------------------------------------  EVENT HANDLERS ----------------------------------------
;-------------------------------------------------------------------------------------------------
;-------------------------------------------------------------------------------------------------
; #FUNCTION# =======================================================================================================================================================
; Name...........: _Register_WM_NOTIFY
; Description ...: Hook or unhook the WM_NOTIFY Windows event
; Syntax.........: _Register_WM_NOTIFY([$bEnable])
; Parameters ....: $bEnable - Hook or unhook the WM_NOTIFY Windows event (True/False)
; Requirement(s).:
; Return values .:
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _Register_WM_NOTIFY($bEnable=True)
   If ($bEnable) Then
	  GUIRegisterMsg($WM_NOTIFY, "_WM_NOTIFY")
   Else
	  GUIRegisterMsg($WM_NOTIFY, "")
   EndIf
EndFunc   ;==>_Register_WM_NOTIFY

; #FUNCTION# =======================================================================================================================================================
; Name...........: _WM_NOTIFY
; Description ...: Hook or unhook the WM_NOTIFY Windows event
; Syntax.........: _WM_NOTIFY($hWnd, $iMsg, $wParam, $lParam)
; Parameters ....: $hWnd - Window handle of the component that is sending the message
;                  $iMsg - Windows message
;                  $wParam - wParam
;                  $lParam - lParam
; Requirement(s).:
; Return values .:
; Author ........: Xatanú
; Modified.......:
; Remarks .......: Commonly used for controlling clicks in the windows
; Related .......:
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _WM_NOTIFY($hWnd, $iMsg, $wParam, $lParam)
   Local $tNMHDR, $hWndFrom, $IDFrom, $Code, $tNMLISTVIEW, $iItem
   $hWndListView = $hListView
   If Not IsHWnd($hListView) Then $hWndListView = GUICtrlGetHandle($hListView)
   $tNMHDR = DllStructCreate($tagNMHDR, $lParam)
   $hWndFrom = HWnd(DllStructGetData($tNMHDR, "hWndFrom"))
   $Code = DllStructGetData($tNMHDR, "Code")
;~    $IDFrom = DllStructGetData($tNMHDR, "IDFrom")
;~    Local $tInfo = DllStructCreate($tagNMLISTVIEW, $lParam)
;~    Local $iItem = DllStructGetData($tInfo, "Item")

   Switch $hWndFrom
	  Case $hWndListView
		 Switch $Code
			Case -12 ; User has changed column width
			   If (_GUICtrlListView_GetColumnWidth($hWndListView, 0) <> 20) Then
				  _GUICtrlListView_SetColumnWidth($hWndListView, 0, 20) ; width of column 0 reset to 20
			   ElseIf (_GUICtrlListView_GetColumnWidth($hWndListView, 1) <> 0) Then
				  _GUICtrlListView_SetColumnWidth($hWndListView, 1, 0) ; width of column 1 reset to zero
			   ElseIf (_GUICtrlListView_GetColumnWidth($hWndListView, 8) <> 0) Then
				  _GUICtrlListView_SetColumnWidth($hWndListView, 8, 0) ; width of column 7 reset to zero
			   EndIf
			Case $LVN_ITEMCHANGING ; $NM_CLICK, $NM_DBLCLK, $NM_RCLICK, $NM_RDBLCLK
			   $tInfo = DllStructCreate($tagNMITEMACTIVATE, $LParam)
			   ;preventing checkboxes from re-apearing (same conditions as above)
			   $iItem = DllStructGetData($tInfo, "Index")
			   $item_value = _GUICtrlListView_GetItemText($hWndListView, $iItem, 1)
			   If ($item_value == "-1") Then
				  Return True	;intercept normal return message which would cause checkbox to re-apear
			   EndIf
			Case $LVN_ITEMCHANGED ; Item changed
			   $tNMLISTVIEW = DllStructCreate($tagNMLISTVIEW, $lParam)
			   If BitAND(DllStructGetData($tNMLISTVIEW, "Changed"), $LVIF_STATE) = $LVIF_STATE Then
				  $iItem = DllStructGetData($tNMLISTVIEW, "Item")
				  $item_id = _GUICtrlListView_GetItemText($hWndListView, $iItem, 2)
				  $item_display = _GUICtrlListView_GetItemText($hWndListView, $iItem, 3)
				  $item_text = $item_id & " - " & $item_display
				  $item_type = _GUICtrlListView_GetItemText($hWndListView, $iItem, 5)
				  $stock_startup = _GUICtrlListView_GetItemText($hWndListView, $iItem, 7)
				  _WriteLineToLog("Item: " & $item_id & " Item_type: " & $item_type & " Stock_startup: " & $stock_startup, $LOG_DEBUG)
				  $bChanged = False
				  Switch DllStructGetData($tNMLISTVIEW, "NewState")
					 Case 8192 ;item checked
						_WriteLineToLog('Item ' & $iItem & ' - ' & $item_text & ' - ' & True, $LOG_DEBUG)
						If (_EnableItem($item_id, $item_type, $stock_startup)) Then
						   $bChanged = True
						Else
						   SetError(1)
						EndIf
					 Case 4096 ;item unchecked
						_WriteLineToLog('Item ' & $iItem & ' - ' & $item_text & ' - ' & False, $LOG_DEBUG)
						If (_DisableItem($item_id, $item_type)) Then
						   $bChanged = True
						Else
						   SetError(1)
						EndIf
				  EndSwitch
				  If ($bChanged) Then
					 ; set internal flag for sorting checkboxes
					 _GUICtrlListView_SetItem($hWndListView, 1 - _GUICtrlListView_GetItemChecked($hWndListView, $iItem), $iItem, 1)
					 ; update item
					 _RefreshItem($hWndListView, $iItem, $item_id, $item_type)
				  ElseIf (@error) Then
					 _Register_WM_NOTIFY(False)
					 _GUICtrlListView_SetItemChecked($hListView, $iItem, Not _GUICtrlListView_GetItemChecked($hWndListView, $iItem))
					 _Register_WM_NOTIFY()
				  EndIf
			   EndIf
		 EndSwitch
   EndSwitch
   Return $GUI_RUNDEFMSG
EndFunc   ;==>_WM_NOTIFY

; #FUNCTION# =======================================================================================================================================================
; Name...........: _WM_COMMAND
; Description ...: Hook or unhook the _WM_COMMAND Windows event
; Syntax.........: _WM_COMMAND($hWnd, $Msg, $wParam, $lParam)
; Parameters ....: $hWnd - Window handle of the component that is sending the message
;                  $Msg - Windows message
;                  $wParam - wParam
;                  $lParam - lParam
; Requirement(s).:
; Return values .:
; Author ........: Xatanú
; Modified.......:
; Remarks .......: Commonly used for controlling wich button has been pressed
; Related .......:
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _WM_COMMAND($hWnd, $Msg, $wParam, $lParam)
   Local $hFrom = HWnd($lParam)
   Local $idFrom = BitAND($wParam, 0x0000FFFF)
   Local $iCode = BitShift(BitAND($wParam, 0xFFFF0000), 16)
   If $iCode = $BN_CLICKED Then
	  Switch $idFrom
		 Case $idSaveProfile
		   _SaveProfile()
		 Case $idLoadProfile
		   _LoadProfile()
		 Case $idEnableAll
			_EnableAll()
		 Case $idDisableAll
			_DisableAll()
		 Case $idRestoreDefaults
			_RestoreDefaults()
		 Case $idRefresh
			_RefreshAll()
	  EndSwitch
   EndIf
   $tNMHDR = 0
   Return $GUI_RUNDEFMSG
EndFunc   ;==>_WM_COMMAND

; #FUNCTION# =======================================================================================================================================================
; Name...........: _Terminate
; Description ...: Exits the program
; Syntax.........: _Terminate()
; Parameters ....:
; Requirement(s).:
; Return values .:
; Author ........: Xatanú
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ==================================================================================================================================================================
Func _Terminate()
   ConsoleWrite("Exiting"&@CR)
   GUIDelete($hGUI)
   Exit
EndFunc   ;==>_Terminate