# WBD

Windows Bloat Disabler

1. Description
	This program is intented for disabling the typical unnecessary Windows components.

2. Requirements
	Windows 10 and administration rights.

3. Features
	Supported Windows components:
	- Services
	- Drivers
	- Tasks
	- Optional Features

	Supported actions:
	- Disable a component: Untick the checkbox
	- Enable a component: Tick the checkbox
	- Create a custom file: Simply edit the "components.txt" file and remove or add the desired components. 
	  You can also remove the components.txt file and start the app to force the creating of a new components list.
	  The app will ask which components to include in the file when it starts.
	- Save a profile: Save the current status of enabled and disabled components to a file for later usage
	- Load a profile: Load a previously generated profile and restores the status of the included components
	- Disable all: Disable all the listed components
	- Enable all: Enable all the listed components
	- Default: Revert to stock startup. Enable or disable each component based on the "stock" configuration
	- Refresh: Reload the list of components with their status

	Display sensitive components with a different color in the app (as a warning). There are two supported levels (1, 2)

4. Components file format
	Type;Serviceid;Description;Stock Startup;Warn Level

	- Type: The type of component, choose between "Service", "Driver", "Task" and "Feature"
	- ServiceID: The windows identifier name for the component
	- Description: Optional. You can include a short description for the component or leave it blank (the app will try to get the description from Windows)
	- Stock Startup: Stock status of the component.
		Services and Drivers: "Boot Start", "System Start", "Automatic", "Manual", "Disabled"
		Tasks: "Enabled", "Disabled"
		Features: "Enabled", "Disabled"
	- Warn Level: Used to mark sensitive component with a different color. "0" Disabled, "1" Warn1, "2" Warn2

	Lines starting with # are ignored

5. Color codes
	Grey: Services and drivers that have "manual" (demand) startup and are stopped will be marked in grey (to represent that maybe you don't need to disable them)
	Blue: Components which startup status does not match with the stock one (e.g. when you disable a component it will be marked in blue)
	Yellow: Warn1 - defined by user. Commonly used to represent components that may be required sometime (e.g. print spooler services)
	Orange: Warn2 - defined by user. Commonly used to represent components that are somehow critical or sensitive for some features to work
